#!/usr/bin/python3
#
# 2D vortices
#

import numpy as np
import matplotlib.pyplot as plt

###### Flow definition #########################################################
maxIter = 10000  # Total number of time iterations.
Re = 10.0         # Reynolds number.
nx, ny = 200, 200 # Numer of lattice nodes.
dx = 2*np.pi / ny
uLB     = 0.04                  # Velocity in lattice units.
nulb    = uLB*ny/Re             # Viscoscity in lattice units.
omega = 1 / (3*nulb+0.5)    # Relaxation parameter.
m = 1
n = 1
lamb = -(m**2 + n**2)

###### Lattice Constants #######################################################
v = np.array([ [ 1,  1], [ 1,  0], [ 1, -1], [ 0,  1], [ 0,  0],
            [ 0, -1], [-1,  1], [-1,  0], [-1, -1] ])
t = np.array([ 1/36, 1/9, 1/36, 1/9, 4/9, 1/9, 1/36, 1/9, 1/36])

###### Function Definitions ####################################################
def macroscopic(fin):
    rho = np.sum(fin, axis=0)
    u = np.zeros((2, nx, ny))
    for i in range(9):
        u[0,:,:] += v[i,0] * fin[i,:,:]
        u[1,:,:] += v[i,1] * fin[i,:,:]
    u /= rho
    return rho, u

def equilibrium(rho, u):              # Equilibrium distribution function.
    usqr = 3/2 * (u[0]**2 + u[1]**2)
    feq = np.zeros((9,nx,ny))
    for i in range(9):
        cu = 3 * (v[i,0]*u[0,:,:] + v[i,1]*u[1,:,:])
        feq[i,:,:] = rho*t[i] * (1 + cu + 0.5*cu**2 - usqr)
    return feq

# Initial velocity profile: almost zero, with a slight perturbation to trigger
# the instability.
def rho_0(x, y):
    return 1 - 3 * uLB**2 * ((n / ny)**2 * np.cos(4 * np.pi * m / nx * x) + (m / nx)**2 * np.cos(4 * np.pi*n/ny*y))
def ux_0(x, y):
    return -np.cos(2 * np.pi * m / nx * x) * np.sin(2 * np.pi * n / ny * y) / np.sqrt(m**2/n**2 + 1)
def uy_0(x, y):
    return  np.sin(2 * np.pi * m / nx * x) * np.cos(2 * np.pi * n / ny * y) / np.sqrt(n**2/m**2 + 1)

def ua(u, it, nu, l):
    return np.exp(l * nu * it) * u

def error_sqr(u, v):
    return (u - v)**2

def avg_error(u, v):
    return np.mean(np.sqrt(np.sum(error_sqr(u, v), axis = 0)))

r = np.fromfunction(rho_0, (nx,ny))
vx = np.fromfunction(ux_0, (nx,ny))
vy = np.fromfunction(uy_0, (nx,ny))

v0 = uLB * np.array([vx, vy])

# Initialization of the populations at equilibrium with the given velocity.
fin = equilibrium(r, v0)

errors   = np.array([])
energy   = np.array([])
energy_a = np.array([])
###### Main time loop ##########################################################
for time in range(maxIter):
    # Compute macroscopic variables, density and velocity.
    rho, u = macroscopic(fin)
    u_a = ua(v0, nulb, time, lamb * dx * dx)
    e = avg_error(u_a, u)
    errors = np.append(errors, e)
    en = np.sum(0.5 * np.sum(u_a**2, 0))
    en_a = np.sum(0.5 * np.sum(u**2, 0))
    energy = np.append(energy, en)
    energy_a = np.append(energy_a, en_a)
    # print("en_e = ", en, "en_a = ", en_a)


   # Compute equilibrium.
    feq = equilibrium(rho, u)

    # Collision step.
    fout = fin - omega * (fin - feq)

   # Streaming step.
    for i in range(9):
        fin[i,:,:] = np.roll(
                            np.roll(fout[i,:,:], v[i,0], axis=0),
                            v[i,1], axis=1 )
 
    # Visualization of the velocity.
    if (time%100==0 and time > 0):
        plt.clf()
        pos = plt.imshow(np.sqrt(u[0]**2+u[1]**2).transpose())
        e = error_sqr(ua(v0, nulb, time, lamb * dx * dx), u)
        # pos = plt.imshow(np.sqrt(np.sum(e, axis=0)))
        plt.colorbar(pos)
        # plt.show()
        plt.savefig("tmp/vel.{0:04d}.png".format(time))

plt.clf()
plt.semilogy(np.array(range(maxIter)), energy, energy_a)
plt.show()

