---
author:
- Orestis Malaspinas
title: Physique appliquée
autoSectionLabels: false
autoEqnLabels: true
eqnPrefix:
  - "éq."
  - "éqs."
chapters: true
numberSections: false
chaptersDepth: 1
sectionsDepth: 3
lang: fr
documentclass: book
papersize: A4
cref: false
urlcolor: blue
---

# Tentative simulation

The idea here is to simulate a very simple test case without boundaries and in 
two dimensions. This test case has an analytical solution and can be relatively 
easily be exported into a 3D case with a much richer behavior than the one 
obtained in 2D (in particular turbulent behavior).

The solver is based on the lattice Boltzmann method (which is an  alternative 
to the Navier-Stokes equations). Here we are using the weakly compressible 
regime which is equivalent to the incompressible Navier-Stokes equations in the 
low Mach number approximation.

## Initial condition

The flow here is periodic in every direction and is completely determined by 
its initial condition which is given by
\begin{align}
    \rho_0(x, y)&=-(n^2\cos(4\pi m x)+m^2\cos(4\pi n y)),\\
    u_{x,0}(x, y)&=\frac{-\cos(2\pi m x)\sin(2\pi n 
    y)}{\sqrt{\frac{m^2}{n^2}+1}},\\
    u_{y,0}(x, y)&=\frac{\sin(2\pi m x)\cos(2\pi n 
    y)}{\sqrt{\frac{n^2}{m^2}+1}},
\end{align}
where $m,n\in \mathbb{N}$, and $(x,y)\in [0,1)\times[0,1)$.

## Time evolution

The time evolution of the velocity of the flow is given by
$$
\vec u_a(x,y,t)=\vec u_0\cdot e^{-\lambda\nu t},
$$
where $\nu$ is the kinematic viscosity of the fluid.
The density (or pressure in the incompressible Navier--Stokes equations) can 
also be known at any time by solving the Poisson equation and is given by
$$
\rho_a(x, y, t)=\rho_0(x, y, t)\cdot e^{-\lambda\nu t}.
$$

The parameters of the simulation are as follows.

The flow is simulated in a box $[2\pi m\times 2\pi n]$. We consider the 
characteristic length of the flow to be $L=2\pi m$ and its characteristic 
velocity to be $U=1$. The Reynolds number of the flow therefore gives the 
kinematic viscosity, $\nu$, of the flow through
$$
\mathrm{Re}=\frac{UL}{\nu}\Leftrightarrow \nu=\frac{LU}{\mathrm{Re}}.
$$
The Reynolds number is a **non-dimensional** number of the flow with respect to 
which the Navier-Stokes equations are invariants. Two similar setups with the 
same Reynolds number give the same flow (that's the reason one can use wind 
tunnels to manufacture at lower cost cars or airplanes). 

The numeric parameters, $\delta x$ and $\delta t$ (space and time 
discretization) of the simulation are defined as
\begin{align}
    \delta x&=2\pi m/N,\\
    \delta t&=U_\mathrm{lb}\delta x,\\
\end{align}
where $N$ is the number of grid points in a side of the simulation domain,
and $U_\mathrm{lb}\ll 1/3$ the characteristic velocity of the flow in *lattice 
units*. The Reynolds number in terms of the *lattice units* is then
$$
\mathrm{Re}=\frac{N U_\mathrm{lb}}{\nu_\mathrm{lb}},
$$

## Proposed experiment

### Description of the model 

The simulation is based on the lattice Boltzmann method. The numerical scheme 
is expressed in terms of the BGK-Boltzmann collision model which asymptotically 
leads to the Navier-Stokes equations. The numerical scheme reads
$$
f_i(\vec x+\vec c_i\delta t,t+\delta t)=f_i(\vec 
x,t)-\frac{1}{\tau}\left(f_i(\vec x, t)-f_i^\mathrm{eq}(\vec x, t)\right),
$$
where $\tau$ is the relaxation time which is related to the kinematic viscosity
$$
\nu_\mathrm{lb}=\frac{1}{3}\left(\tau-\frac{1}{2}\right).
$$
The equilibrium distribution function
$$
f_i^\mathrm{eq}(\vec x, t)=w_i\rho(\vec x, t)\left(1+3\frac{\vec c_i\cdot \vec 
u(\vec x, t)}+\frac{9}{2}(\vec c_i\vec c_i-\underline{\underline{I}}):\vec 
u(\vec x,t)\vec u(\vec x,t)\right)
$$
is only function of the density, $\rho(\vec x,t)$ and the velocity $\vec u(\vec 
x,t)$ ($w_i$ and $\vec c_i$ are parameters of the model) which are given by
$$
\rho(\vec x,t)=\sum_i f(\vec x, t),\quad \rho(\vec x, t)\vec u(\vec x, 
t)=\sum_i\vec c_i f_i(\vec x,t).
$$

### Error determination

The numerical model is subject to a discretization errors, as well as "modeling 
error", namely the compressibility error. While the numerical error is related 
to the time-space discretization, the modeling error is related to the nature 
of the equation which is only asymptotically equivalent to the incompressible
Navier--Stokes equations adding an extra error term.

At each time-step the error between the exact solution and the numerical 
simulation can be evaluated on each grid point with respect to the velocity
$$
E_u(\vec x, t)=\frac{1}{N^2}\sqrt{\sum_{j=0}^{N-1}\sum_{k=0}^{N-1}||\vec 
u(x_{jk},t)-\vec u_a(x_{jk}, t)||^2}
$$
and to the density
$$
E_\rho(\vec x, 
t)=\frac{1}{N^2}\sqrt{\sum_{j=0}^{N-1}\sum_{k=0}^{N-1}||\rho(x_{jk},t)-\rho_a(x_{jk}, 
t)||^2}.
$$


